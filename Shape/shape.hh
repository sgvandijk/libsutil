#ifndef __SUTIL_SHAPE_HH_
#define __SUTIL_SHAPE_HH_

#include <string>
#include <vector>
#include "../Color/color.hh"
#include <memory>

namespace sutil 
{
  struct Size
  {
    static bool useAbs;

    Size() : val(0), abs(useAbs) {}
    Size(double v, bool a = useAbs) : val(v), abs(a) {}
    double val;
    bool abs;
    
    Size operator+(Size const& other) const { return Size(val + other.val); }
    Size operator-(Size const& other) const { return Size(val - other.val); }
    Size operator-() const { return Size(-val); }
    Size operator*(double v) const { return Size(v * val); }
  };

  struct Shape
  {
    Shape() : stroke("black"), strokeWidth(1), fill(new StrColor("white")), style(""),opacity(1),x(0),y(0),scale(1) {}
    virtual ~Shape() {}
    
    std::string stroke;
    double strokeWidth;
    std::shared_ptr<Color> fill;
    std::string style;
    std::string pattern;
    double opacity;
    
    Size x;
    Size y;
    
    double scale;
    
    virtual Shape* clone() = 0;
  };

  struct Line : public Shape
  {
    Line() : Shape(), x1(0), y1(0), x2(100), y2(100) {}
    Size x1;
    Size y1;
    Size x2;
    Size y2;
    
    virtual Shape* clone()
    {
      Line* c = new Line();
      *c = *this;
      return c;
    }
  };

  struct Arrow : public Line
  {
    Arrow() : Line(), headSize(5) {}
    Size headSize;

    virtual Shape* clone()
    {
      Arrow* a = new Arrow();
      *a = *this;
      return a;
    }
  };

  struct Rect : public Shape
  {
    Rect() : Shape(), width(100, false), height(100, false) {}
    Size width;
    Size height;

    virtual Shape* clone()
    {
      Rect* c = new Rect();
      *c = *this;
      return c;
    }
  };

  struct Circle : public Shape
  {
    Circle() : Shape(), centrX(50, false), centrY(50, false), radius(25, false) {}
    Size centrX;
    Size centrY;
    Size radius;

    virtual Shape* clone()
    {
      Circle* c = new Circle();
      *c = *this;
      return c;
    }
  };

  struct Polygon : public Shape
  {
    Polygon() : Shape(), points() {}
    std::vector<std::pair<Size, Size> > points;

    virtual Shape* clone()
    {
      Polygon* c = new Polygon();
      *c = *this;
      return c;
    }
  };

  struct Polyline : public Shape
  {
    Polyline() : Shape(), points() {}
    std::vector<std::pair<Size, Size> > points;

    virtual Shape* clone()
    {
      Polyline* c = new Polyline();
      *c = *this;
      return c;
    }
  };

  struct Text : public Shape
  {
    Text()
    : Shape(),
      fontFamily("Verdana"), fontSize(14), content(""),
      textAnchor(""), alignmentBaseline("")
    {
      fill = std::shared_ptr<Color>(new StrColor("black"));
      stroke = "none";
    }
    
    std::string fontFamily;
    Size fontSize;
    std::string content;
    std::string textAnchor;
    std::string alignmentBaseline;
    std::string baselineShift;
    
    virtual Shape* clone()
    {
      Text* c = new Text();
      *c = *this;
      return c;
    }
  };

}

#endif
  
