#ifndef _SUTIL_VECTORDISTCOMPARE_HH_
#define _SUTIL_VECTORDISTCOMPARE_HH_

#include "../priorityqueue.hh"
#include "../../Vector/vector.hh"

namespace sutil
{
  template<typename T>
  class VectorDistCompare : public PriorityCompare<Vector<T> >
  {
    private:
      Vector<T> d_ref;
      
    public:
      VectorDistCompare(Vector<T> const& ref) : PriorityCompare<Vector<T> >(), d_ref(ref) {}
      virtual double compare(Vector<T> const& a, Vector<T> const& b)
      {
        return ((a - d_ref).length() - (b - d_ref).length());
      }
  };
};

#endif

