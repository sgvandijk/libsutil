#ifndef _SUTIL_PRIORITYQUEUE_
#define _SUTIL_PRIORITYQUEUE_

#include <queue>

namespace sutil
{
  template<typename T>
  class PriorityCompare
  {
    public:
      PriorityCompare() {}
      virtual ~PriorityCompare() {}
      
      virtual double compare(T const& a, T const& b) = 0;
  };
  
  template<typename T>
  class PriorityQueueElement
  {
    private:
      T                   d_element;
      PriorityCompare<T>* d_comp;
      
    public:
      PriorityQueueElement(T const& e, PriorityCompare<T>* comp) : d_element(e), d_comp(comp) {}
      
      bool operator<(PriorityQueueElement<T> const& other) const { return d_comp->compare(d_element, other.get()) < 0; }
      
      T get() const { return d_element; }
  };
  
  template<typename T>
  class PriorityQueue : public std::priority_queue<PriorityQueueElement<T> >
  {
    private:
      PriorityCompare<T>* d_comp;
      
    public:
      PriorityQueue(PriorityCompare<T>* comp)
      : std::priority_queue<PriorityQueueElement<T> >(),
        d_comp(comp)
      {}
      
      void push(T& e)
      {
        std::priority_queue<PriorityQueueElement<T> >::push(PriorityQueueElement<T>(e, d_comp));
      }
      
      void push(T e)
      {
        std::priority_queue<PriorityQueueElement<T> >::push(PriorityQueueElement<T>(e, d_comp));
      }
      
      T top()
      {
        return std::priority_queue<PriorityQueueElement<T> >::top().get();
      }
  };
};

#endif
