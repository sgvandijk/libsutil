#ifndef __SUTIL_DEFINITION_HH_
#define __SUTIL_DEFINITION_HH_

namespace sutil 
{
  struct Definition
  {
    Definition() : id("") {}
    virtual ~Definition() {}
    
    std::string id;
    
    virtual Definition* clone() = 0;
  };
  
  struct GradientStop
  {
    Size offset;
    std::string color;
    double opacity;
  };
  
  struct Gradient : public Definition
  {
    Gradient() : Definition() {}
    std::vector<GradientStop> stops;
    
    virtual Definition* clone()
    {
      Gradient* c = new Gradient();
      *c = *this;
      return c;
    }
  };
  
  struct LinearGradient : public Gradient
  {
    LinearGradient() : Gradient(), x1(0, false), y1(0, false), x2(100, false), y2(100, false) {}
    Size x1;
    Size y1;
    Size x2;
    Size y2;
  };
  
  struct RadialGradient : public Gradient
  {
    RadialGradient() : Gradient(), centrX(50,false), centrY(50, false), radius(50, false) {}
    Size centrX;
    Size centrY;
    Size radius;
  };
  
  struct Pattern : public Definition
  {
    Pattern() : Definition(), patternUnits("userSpaceOnUse"), x(0), y(0), width(10), height(10) {}
    std::string patternUnits;
    Size x;
    Size y;
    Size width;
    Size height;
    std::vector<Shape*> shapes;

    virtual Pattern* clone()
    {
      Pattern* c = new Pattern();
      *c = *this;
      return c;
    }
  };
}

#endif

