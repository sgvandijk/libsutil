#ifndef _SUTIL_DEBUGSTREAM_HH_
#define _SUTIL_DEBUGSTREAM_HH_

#include <ostream>
#include <fstream>
#include <vector>

#define DbgNone     DebugStream::Level(0)
#define DbgLow      DebugStream::Level(1)
#define DbgMedium   DebugStream::Level(2)
#define DbgHigh     DebugStream::Level(3)
#define DbgPush(C)  DebugStream::PushTrace(C)
#define DbgPop      DebugStream::PopTrace()
#define DbgTrace    DebugStream::PrintTrace()

namespace sutil
{
  class DebugStream : public std::ostream
  {
    public:
      struct Modifier
      {
        protected:
          Modifier() {}
          virtual ~Modifier() {}
      };
      
      struct Level : public Modifier
      {
        unsigned val;
        Level(unsigned v) : val(v) {}
      };
      
      struct PushTrace : public Modifier
      {
        std::string str;
        PushTrace() : str("") {}
        PushTrace(std::string const& s) : str(s) {}
      };
      
      struct PopTrace : public Modifier
      {
      };
      
      struct PrintTrace : public Modifier
      {
      };
      
      enum Mode
      {
        StdErr = 0,
        File
      };
      
    private:
      std::streambuf* d_lastStreambuf;
      
      std::ofstream d_fileOut;
      
      Level d_level;
      Mode d_mode;
      
      Level d_curLevel;
      
      std::vector<std::string> d_trace;
      
      std::string d_filter; 
      bool d_on;
      
      static DebugStream* s_instance;
      
      void on();
      void off();
      
      DebugStream(int argc, char const** argv);
    public:
      DebugStream();
      
      Level getLevel() const { return d_level; }
      void setLevel(Level level)
      {
        d_level = level;
        if (d_curLevel.val > d_level.val)
          off();
        else
          on();
      }
      
      void setStdErrMode();
      void setFileMode(std::string file);
      void setFilter(std::string const& filter);
      
      bool isOn() const {return d_on;}
      
      static void initialize(int argc, char const** argv);
      static DebugStream& getInstance() { return *s_instance; }

      friend DebugStream& operator<<(DebugStream& dbg, Modifier const& mod);
  };

  DebugStream& operator<<(DebugStream& dbg, DebugStream::Modifier const& mod);
};

#endif
