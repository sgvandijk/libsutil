#include "debugstream.ih"

DebugStream& sutil::operator<<(DebugStream& dbg, DebugStream::Modifier const& mod)
{

  DebugStream::Level const* level = dynamic_cast<DebugStream::Level const*>(&mod);
  if (level)
  {
    dbg.d_curLevel = *level;
    if (level->val > dbg.d_level.val)
      dbg.off();
    else
      dbg.on();
      
    return dbg;
  }
  
  DebugStream::PushTrace const* trace = dynamic_cast<DebugStream::PushTrace const*>(&mod);
  if (trace)
  {
    dbg.d_trace.push_back(trace->str);
    if (trace->str == dbg.d_filter && dbg.d_curLevel.val <= dbg.d_level.val)
      dbg.on();
    return dbg;
  }
  
  if(dynamic_cast<DebugStream::PopTrace const*>(&mod))
  {
    if (dbg.d_trace.back() == dbg.d_filter)
      dbg.off();
      
    dbg.d_trace.pop_back();
    return dbg;
  }
  
  if (!dbg.isOn())
    return dbg;
    
  if(dynamic_cast<DebugStream::PrintTrace const*>(&mod))
  {
    for (vector<string>::iterator iter = dbg.d_trace.begin(); iter != dbg.d_trace.end(); ++iter)
      dbg << *iter << " > ";
    return dbg;
  }
  
  return dbg;
}

