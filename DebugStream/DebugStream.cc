#include "debugstream.ih"

DebugStream::DebugStream()
: ostream(0),
  d_lastStreambuf(0),
  d_level(DbgMedium),
  d_mode(StdErr),
  d_curLevel(DbgMedium)
{
  d_lastStreambuf = cerr.rdbuf();
  rdbuf(cerr.rdbuf());
}


DebugStream::DebugStream(int argc, char const** argv)
: ostream(0),
  d_lastStreambuf(0),
  d_level(DbgMedium),
  d_mode(StdErr),
  d_curLevel(DbgMedium)
{
  string file("debug.txt");
  string filter;
  
  for (int i = 0; i < argc; ++i)
  {
    string opt(argv[i]);
    if (opt.substr(0, 3) == "-dm")
      d_mode = (Mode)atoi(opt.substr(3).c_str());
    else if (opt.substr(0,3) == "-dl")
      d_level = Level(atoi(opt.substr(3).c_str()));
    else if (opt == "-do")
      file = argv[++i];
    else if (opt == "-df")
      filter = argv[++i];
  }
  
  switch (d_mode)
  {
    case StdErr:
      d_lastStreambuf = cerr.rdbuf();
      rdbuf(cerr.rdbuf());
      break;
      
    case File:
      d_fileOut.open(file.c_str());
      d_lastStreambuf = d_fileOut.rdbuf();
      rdbuf(d_fileOut.rdbuf());
      break;
  }

  if (d_curLevel.val > d_level.val)
    off();

  setFilter(filter);
}

