#include "debugstream.ih"

void DebugStream::setFilter(string const& filter)
{
  d_filter = filter;
  if (filter == "" && d_curLevel.val <= d_level.val)
    on();
  else
  {
    off();
    for (vector<string>::iterator iter = d_trace.begin(); iter != d_trace.end(); ++iter)
      if (*iter == filter && d_curLevel.val <= d_level.val)
        on();
  }
}
