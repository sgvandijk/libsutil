#include "debugstream.ih"

void DebugStream::off()
{
  d_on = false;
  streambuf* lastBuf = rdbuf(0);
  if (lastBuf)
    d_lastStreambuf = lastBuf;
}

