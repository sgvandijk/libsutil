#ifndef __SUTIL_COLOR_HH_
#define __SUTIL_COLOR_HH_

namespace sutil
{
  struct Color
  {
    virtual ~Color() {}
  };
  
  struct StrColor : public Color
  {
    StrColor(std::string const& s) : str(s) {}
    
    std::string str;
  };
  
  struct RGBColor : public Color
  {
    RGBColor(double _r, double _g, double _b) : r(_r), g(_g), b(_b) {}
    
    double r;
    double g;
    double b;
  };
  
  struct HSVColor : public Color
  {
    HSVColor(double _h, double _s, double _v) : h(_h), s(_s), v(_v) {}
    
    double h;
    double s;
    double v;
  };
}

#endif
