#ifndef _SUTIL_MATRIX_HH_
#define _SUTIL_MATRIX_HH_

#include "../Vector/vector.hh"
#include <ostream>

namespace sutil
{
  template<typename T>
	class Matrix
	{
	  protected:
	    Vector<T> *d_data;
	    unsigned d_nRows;
	    unsigned d_nCols;
	    
	    void copy(Matrix<T> const& other);
	    void copy(Vector<T> const& vec);
	    void destroy();
	    
	  public:
	    Matrix<T>();
	    Matrix<T>(unsigned nRows, unsigned nCols);
	    Matrix<T>(Matrix<T> const& other);
	    Matrix<T>(Vector<T> const& vec);
	    ~Matrix<T>();
	    
	    Matrix<T>& operator=(Matrix<T> const& other);
	    Matrix<T>& operator=(Vector<T> const& vec);
	    
      operator Vector<T>() const;
      
	    Vector<T> operator[](unsigned idx) const;
	    Vector<T>& operator[](unsigned idx);
	    
	    unsigned rows() const;
	    unsigned cols() const;
	    
	    Matrix<T> operator+(T val) const;
	    Matrix<T> operator-(T val) const;
	    Matrix<T> operator*(T val) const;
	    Matrix<T> operator/(T val) const;
	    
	    Matrix<T> operator+(Matrix<T> const& other) const;
	    Matrix<T> operator-(Matrix<T> const& other) const;
	    
	    Matrix<T> operator*(Matrix<T> const& other) const;
	    Vector<T> operator*(Vector<T> const& vec) const;
	    
	    Matrix<T> transpose() const;
	    Matrix<T> gaussJordanElimination() const;
	    Matrix<T> inverse() const;
	    
	    bool inHull(Vector<T> const& vec) const;
	    
	    static Matrix<T> identity(unsigned size);
	};
	
	////////////////////////////////////////
	// Private member functions:
  template<typename T>
	inline void Matrix<T>::copy(Matrix<T> const& other)
	{
	  d_nRows = other.d_nRows;
	  d_nCols = other.d_nCols;
	  d_data = new Vector<T>[d_nRows];
	  for (unsigned i = 0; i < d_nRows; ++i)
	    d_data[i] = other[i];
	}
	
  template<typename T>
	inline void Matrix<T>::copy(Vector<T> const& vec)
	{
	  d_nRows = vec.size();
	  d_nCols = 1;
	  d_data = new Vector<T>[d_nRows];
	  for (unsigned i = 0; i < d_nRows; ++i)
	  {
	    d_data[i] = Vector<T>(1);
	    d_data[i][0] = vec[i];
	  }
	}
	
  template<typename T>
	inline void Matrix<T>::destroy()
	{
	  if (d_data)
	  {
	    delete[] d_data;
	    d_data = 0;
	  }
	}

	////////////////////////////////////////
	// Public member functions:
  template<typename T>
	inline Matrix<T>::Matrix()
	: d_data(0), d_nRows(0), d_nCols(0)
	{}
	
  template<typename T>
	inline Matrix<T>::Matrix(unsigned nRows, unsigned nCols)
	: d_data(0), d_nRows(nRows), d_nCols(nCols)
	{
	  d_data = new Vector<T>[d_nRows * d_nCols];
	  for (unsigned i = 0; i < d_nRows; ++i)
	    d_data[i] = Vector<T>(d_nCols);
	}
	
  template<typename T>
	inline Matrix<T>::Matrix(Matrix<T> const& other)
	{
	  copy(other);
	}
	
  template<typename T>
	inline Matrix<T>::Matrix(Vector<T> const& vec)
	{
	  copy(vec);
	}
	
  template<typename T>
	inline Matrix<T>::~Matrix()
	{
	  destroy();
	}
	
  template<typename T>
	inline Matrix<T>& Matrix<T>::operator=(Matrix<T> const& other)
	{
	  if (&other != this)
	  {
	    destroy();
	    copy(other);
	  }
	  return *this;
	}
	
  template<typename T>
	inline Matrix<T>& Matrix<T>::operator=(Vector<T> const& vec)
	{
    destroy();
    copy(vec);
	  return *this;
	}
	

  template<typename T>
  inline Matrix<T>::operator Vector<T>() const
  {
    assert(d_nCols == 1);
    Vector<T> vec(d_nRows);
    for (unsigned i = 0; i < d_nRows; ++i)
      vec[i] = d_data[i][0];
    return vec;
  }
  
  template<typename T>
	inline Vector<T> Matrix<T>::operator[](unsigned idx) const
	{
	  return d_data[idx];
	}

  template<typename T>
	inline Vector<T>& Matrix<T>::operator[](unsigned idx)
	{
	  return d_data[idx];
	}
	
  
  template<typename T>
  inline unsigned Matrix<T>::rows() const
  {
    return d_nRows;
  }
  
  template<typename T>
  inline unsigned Matrix<T>::cols() const
  {
    return d_nCols;
  }
  
  
  template<typename T>
	inline Matrix<T> Matrix<T>::operator+(T val) const
	{
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] + val;
	  return res;
	}

  template<typename T>
	inline Matrix<T> Matrix<T>::operator-(T val) const
	{
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] - val;
	  return res;
	}

  template<typename T>
	inline Matrix<T> Matrix<T>::operator*(T val) const
	{
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] * val;
	  return res;
	}

  template<typename T>
	inline Matrix<T> Matrix<T>::operator/(T val) const
	{
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] / val;
	  return res;
	}
	
  
  template<typename T>
  inline Matrix<T> Matrix<T>::operator+(Matrix<T> const& other) const
	{
	  assert(d_nRows == other.d_nRows && d_nCols == other.d_nCols);
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] + other[i];
	  return res;
	}

  template<typename T>
  inline Matrix<T> Matrix<T>::operator-(Matrix<T> const& other) const
	{
	  assert(d_nRows == other.d_nRows && d_nCols == other.d_nCols);
	  Matrix<T> res(d_nRows, d_nCols);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    res[i] = (*this)[i] - other[i];
	  return res;
	}
	
	
  template<typename T>
	inline Matrix<T> Matrix<T>::operator*(Matrix<T> const& other) const
	{
	  assert(d_nCols == other.d_nRows);
	  Matrix<T> res(d_nRows, other.d_nCols);
	  for (unsigned i = 0; i < res.d_nRows; ++i)
	    for (unsigned j = 0; j < res.d_nCols; ++j)
	    {
	      double val = 0;
	      for (unsigned k = 0; k < d_nCols; ++k)
	        val += (*this)[i][k] * other[k][j];
	      res[i][j] = val;
	    }
	  return res;
	}
	
  template<typename T>
	inline Vector<T> Matrix<T>::operator*(Vector<T> const& vec) const
	{
		assert(d_nCols == vec.size());
		Vector<T> res(d_nRows);
		for (unsigned i = 0; i < d_nRows; ++i)
			for (unsigned j = 0; j < d_nCols; ++j)
				res[i] += (*this)[i][j] * vec[j];
		return res;
	}
	
  template<typename T>
	inline Matrix<T> Matrix<T>::transpose() const
	{
	  Matrix<T> trans(d_nCols, d_nRows);
	  for (unsigned i = 0; i < d_nRows; ++i)
	    for (unsigned j = 0; j < d_nCols; ++j)
	      trans[j][i] = (*this)[i][j];
	  return trans;
	}
	
  template<typename T>
  Matrix<T> Matrix<T>::gaussJordanElimination() const
  {
    Matrix<T> res(*this);
    
    for (unsigned r = 0; r < d_nRows; ++r)
    {
      // Find next row with a leading number
      for (unsigned i = r; i < d_nRows; ++i)
      {
        if (res[i][r] > 0)
        {
          Vector<T> top = res[r];
          Vector<T> row = res[i];
            
          // switch with top row
          
          res[i] = top;
        
          // make leading 1
          row = row / row[r];

          res[r] = row;
        
          // make all other rows 0
          for (unsigned j = 0; j < d_nRows; ++j)
          {
            if (j == r)
              continue;
            res[j] = res[j] - row * res[j][r];
          }
        
          // we're done when we found 1 row
          break;
        }
      }
    }

    return res;
  }

  template<typename T>
  Matrix<T> Matrix<T>::inverse() const
  {
    Matrix<T> res(*this);
    Matrix<T> ext1(d_nRows, d_nCols + d_nRows);
    
    for (unsigned i = 0; i < d_nRows; ++i)
      for (unsigned j = 0; j < d_nCols + d_nRows; ++j)
        ext1[i][j] = (j < d_nCols ? res[i][j] : (i + d_nCols == j ? 1 : 0));

    Matrix<T> ext2 = ext1.gaussJordanElimination();
    for (unsigned i = 0; i < d_nRows; ++i)
      for (unsigned j = 0; j < d_nCols; ++j)
        res[i][j] = ext2[i][j + d_nRows];
    
    return res;
  }

  template<typename T>
  bool Matrix<T>::inHull(Vector<T> const& vec) const
  {
    assert(vec.size() == d_nCols);
    
    Matrix<T> matT = transpose();
    Matrix<T> prod = matT * *this;
    Matrix<T> prodInv = prod.inverse();
    Matrix<T> hat = *this * prodInv * matT;
    
    // Get highest value on the diagonal
    double hatMax = -1000000;
    for (unsigned i = 0; i < d_nRows; ++i)
      if (hat[i][i] > hatMax)
        hatMax = hat[i][i];
    
        
    // Check if state is inside of the hull
    double val = (((Matrix<T>)vec).transpose() * prodInv * (Matrix<T>)vec)[0][0];
    
    return hatMax > val;

  }

  template<typename T>
  Matrix<T> Matrix<T>::identity(unsigned size)
  {
    Matrix<T> mat(size, size);
    for (unsigned i = 0; i < size; ++i)
      mat[i][i] = 1;
    return mat;
  }
  
  ////////////////////////////////////////
	// Global functions:
  template<typename T>
  std::ostream& operator<<(std::ostream& out, Matrix<T> const& mat)
  {
    for (unsigned i = 0; i < mat.rows(); ++i)
      out << "|" << mat[i] << "|" << std::endl;
    return out;
  }
  
  template<typename T>
  Matrix<T> operator+(T val, Matrix<T> const& mat)
  {
    return mat + val;
  }
  
  template<typename T>
  Matrix<T> operator-(T val, Matrix<T> const& mat)
  {
    return mat - val;
  }
  
  template<typename T>
  Matrix<T> operator*(T val, Matrix<T> const& mat)
  {
    return mat * val;
  }
};

#endif

