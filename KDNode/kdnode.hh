#ifndef _SUTIL_KDNODE_HH_
#define _SUTIL_KDNODE_HH_

#include <armadillo>
#include <vector>

namespace sutil
{
  class KDNode
  {
    public:
      arma::vec d_minBounds;
      arma::vec d_maxBounds;
      
    public:
      KDNode() {}
      virtual ~KDNode() {}
      
      arma::vec getMinBounds() { return d_minBounds; }
      void setMinBounds(arma::vec const& bounds) { d_minBounds = bounds; }
      
      arma::vec getMaxBounds() { return d_maxBounds; }
      void setMaxBounds(arma::vec const& bounds) { d_maxBounds = bounds; }
  };
  
  class KDInternalNode : public KDNode
  {
    private:
      KDNode* d_children[2];
    
      unsigned d_splitDimension;
      double d_splitValue;
    
    public:
      KDInternalNode()
      : KDNode()
      {
      }
      
      void setSplitDimension(unsigned dimension) { d_splitDimension = dimension; }
      unsigned getSplitDimension() { return d_splitDimension; }
      
      void setSplitValue(double value) { d_splitValue = value; }
      double getSplitValue() { return d_splitValue; }
      
      void setChildren(KDNode* left, KDNode* right)
      {
        d_children[0] = left;
        d_children[1] = right;
      }
      
      void setChild(KDNode* node, unsigned side) { d_children[side] = node; }
      
      KDNode* getLeft() { return d_children[0]; }
      KDNode* getRight() { return d_children[1]; }
  };
  
  class KDLeafNode : public KDNode
  {
    private:
      std::vector<arma::vec> d_data;
    
    public:
      KDLeafNode() : KDNode() {}
      
      std::vector<arma::vec > getData() const { return d_data; }
      arma::vec getData(unsigned idx) const { return d_data[idx]; }
      
      unsigned getDataSize() const { return d_data.size(); }
      
      void addData(arma::vec const& data) { d_data.push_back(data); }
  };
};

#endif

