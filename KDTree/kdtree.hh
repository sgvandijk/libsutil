#ifndef _SUTIL_KDTREE_HH_
#define _SUTIL_KDTREE_HH_

#include "../KDNode/kdnode.hh"
#include "../PriorityQueue/priorityqueue.hh"

namespace sutil
{
  class KDTree
  {
    private:
      KDNode* d_root;
      unsigned d_dataSize;
      unsigned d_splitSize;
      
      unsigned d_size;
      
      void insert(arma::vec const& data, KDNode* node, KDInternalNode* parent, unsigned side);
      void query(PriorityQueue<arma::vec >& nearest, arma::vec const& q, unsigned k, double& tSqr, KDNode* node);
      
      KDInternalNode* split(KDLeafNode* node);
      
    public:
      KDTree();
      KDTree(unsigned dataSize, unsigned splitSize, arma::vec const& minBounds, arma::vec const& maxBounds);
      
      void setDataSize(unsigned dataSize) { d_dataSize = dataSize; }
      void setSplitSize(unsigned splitSize) { d_splitSize = splitSize; }
      void setBounds(arma::vec const& minBounds, arma::vec const& maxBounds)
      {
        d_root->setMinBounds(minBounds);
        d_root->setMaxBounds(maxBounds);
      }
      
      KDNode* getRoot() { return d_root; }
      
      void insert(arma::vec const& data);
      void query(PriorityQueue<arma::vec >& nearest, arma::vec const& q, unsigned k, double t);
      
      unsigned size() { return d_size; }
  };

//--------------------------------------------------------------
// private member functions
//--------------------------------------------------------------

  void KDTree::insert(arma::vec const& data, KDNode* node, KDInternalNode* parent, unsigned side)
  {
    if (KDLeafNode* leaf = dynamic_cast<KDLeafNode*>(node))
    {
      if (leaf->getDataSize() < d_splitSize)
        leaf->addData(data);
      else
      {
        KDInternalNode* internal = split(leaf);

        if (parent)
          parent->setChild(internal, side);
        else
          d_root = internal;

        delete leaf;
        insert(data, internal, parent, side);
      }
    }
    else
    {
      KDInternalNode* internal = dynamic_cast<KDInternalNode*>(node);
      if (data[internal->getSplitDimension()] < internal->getSplitValue())
        insert(data, internal->getLeft(), internal, 0);
      else
        insert(data, internal->getRight(), internal, 1);
    }
  }

  void KDTree::query(PriorityQueue<arma::vec >& nearest, arma::vec const& q, unsigned k, double& tSqr, KDNode* node)
  {
    // Determine if there could be nearer points in this node
    double distSqr = 0;
    for (unsigned i = 0; i < d_dataSize; ++i)
    {
      double min = node->getMinBounds()[i];
      double max = node->getMaxBounds()[i];
      if (q[i] < min)
      {
        double diff = q[i] - min;
        distSqr += diff * diff;
      }
      else if (q[i] > max)
      {
        double diff = max - q[i];
        distSqr += diff * diff;
      }
    }
    
    // Abandon if node is too far away from query
    if (distSqr > tSqr)
      return;
    
    // If this is a leaf node, 
    KDLeafNode* leaf;
    if ((leaf = dynamic_cast<KDLeafNode*>(node)))
    {
      for (unsigned i = 0; i < leaf->getDataSize(); ++i)
        if ((q - leaf->getData(i)).lengthSqr() < tSqr)
          nearest.push(leaf->getData(i));
      
      while (nearest.size() > k)
        nearest.pop();
        
      if (nearest.size() == k)
       tSqr = (nearest.top() - q).lengthSqr();
    }
    else
    {
      KDInternalNode* internal = dynamic_cast<KDInternalNode*>(node);
      if (q[internal->getSplitDimension()] < internal->getSplitValue())
      {
        query(nearest, q, k, tSqr, internal->getLeft());
        query(nearest, q, k, tSqr, internal->getRight());
      }
      else
      {
        query(nearest, q, k, tSqr, internal->getRight());
        query(nearest, q, k, tSqr, internal->getLeft());
      }
    }
  }

  KDInternalNode* KDTree::split(KDLeafNode* node)
  {
    // Create new nodes
    KDInternalNode* internal = new KDInternalNode();
    KDLeafNode* left = new KDLeafNode();
    KDLeafNode* right = new KDLeafNode();
    
    // Set children
    internal->setChildren(left, right);
    
    // Set internal node's bounds
    arma::vec minBounds = node->getMinBounds();
    arma::vec maxBounds = node->getMaxBounds();
    
    internal->setMinBounds(minBounds);
    internal->setMaxBounds(maxBounds);
    
    // Calculate dimension variances
    arma::vec Ex(d_dataSize);
    arma::vec ExSqr(d_dataSize);
    
    for (unsigned i = 0; i < node->getDataSize(); ++i)
    {
      arma::vec data = node->getData(i);
      arma::vec dataSqr = data;
      for (unsigned j = 0; j < dataSqr.size(); ++j)
        dataSqr[j] = dataSqr[j] * dataSqr[j];
      
      Ex = Ex + data;
      ExSqr = ExSqr + dataSqr;
      
    }

    Ex = Ex / node->getDataSize();
    ExSqr = ExSqr / node->getDataSize();
    
    arma::vec SqrEx(Ex.size());
    for (unsigned i = 0; i < d_dataSize; ++i)
      SqrEx[i] = Ex[i] * Ex[i];
      
    arma::vec variance = ExSqr - SqrEx;
    
    // Determine highest variance
    double maxVar = 0;
    unsigned dimension = 0;
    for (unsigned i = 0; i < d_dataSize; ++i)
      if (variance[i] > maxVar)
      {
        maxVar = variance[i];
        dimension = i;
      }

    // Set split values    
    internal->setSplitDimension(dimension);
    internal->setSplitValue(Ex[dimension]);
    
    // Set leaf bounds
    arma::vec leftMaxBounds = maxBounds;
    arma::vec rightMinBounds = minBounds;
    
    leftMaxBounds[dimension] = rightMinBounds[dimension] = Ex[dimension];
    left->setMinBounds(minBounds);
    left->setMaxBounds(leftMaxBounds);
    right->setMinBounds(rightMinBounds);
    right->setMaxBounds(maxBounds);
    
    // Add vectors to correct nodes
    for (unsigned i = 0; i < node->getDataSize(); ++i)
    {
      arma::vec data = node->getData(i);
      if (data[dimension] < Ex[dimension])
        left->addData(data);
      else
        right->addData(data);
    }
    
    return internal;
  }

//--------------------------------------------------------------
// public member functions
//--------------------------------------------------------------

  KDTree::KDTree()
  : d_dataSize(0),
    d_splitSize(0),
    d_size(0)
  {
   d_root = new KDLeafNode();
  }

  KDTree::KDTree(unsigned dataSize, unsigned splitSize, arma::vec const& minBounds, arma::vec const& maxBounds)
  : d_dataSize(dataSize),
    d_splitSize(splitSize),
    d_size(0)
  {
    d_root = new KDLeafNode();
    d_root->setMinBounds(minBounds);
    d_root->setMaxBounds(maxBounds);
  }

  void KDTree::insert(arma::vec const& data)
  {
    insert(data, d_root, 0, 0);
    ++d_size;
  }

  void KDTree::query(PriorityQueue<arma::vec >& nearest, arma::vec const& q, unsigned k, double t)
  {
    double tSqr = t * t;
    query(nearest, q, k, tSqr, d_root);
  }

};

#endif

