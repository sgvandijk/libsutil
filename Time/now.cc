#include "time.ih"

double Time::now()
{
  timeval tv;
  gettimeofday(&tv, 0);
  double t = tv.tv_sec + (double)tv.tv_usec / 1000000.0;
  return t;
}

