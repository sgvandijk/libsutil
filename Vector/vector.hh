#ifndef _SUTIL_VECTOR_HH_
#define _SUTIL_VECTOR_HH_

#include <cassert>
#include <cstring>
#include <ostream>
#include <cmath>
#include <iomanip>

namespace sutil
{
  template<typename T>
	class Vector
	{
	  protected:
			T* d_data;
			unsigned d_size;
			
			void copy(Vector<T> const& other);
			void destroy();
		public:
		  Vector<T>();
			Vector<T>(unsigned size);
			Vector<T>(Vector<T> const& other);
			
			~Vector<T>();
			
			Vector<T>& operator=(Vector<T> const& other);
			
			bool operator==(Vector<T> const& other) const;
			bool operator!=(Vector<T> const& other) const;
			
			T operator[](unsigned idx) const;
			T& operator[](unsigned idx);
			
			unsigned size() const;
			
			bool operator<(Vector<T> const& other) const;
			
			Vector<T> operator+(T val) const;
			Vector<T> operator-(T val) const;
			Vector<T> operator*(T val) const;
			Vector<T> operator/(T val) const;
			
			Vector<T>& operator+=(T val);
			Vector<T>& operator-=(T val);
			Vector<T>& operator*=(T val);
			Vector<T>& operator/=(T val);

			Vector<T> operator+(Vector<T> const& other) const;
			Vector<T> operator-(Vector<T> const& other) const;

			T operator*(Vector<T> const& other) const;
			
			Vector<T>& operator+=(Vector<T> const& other);
			Vector<T>& operator-=(Vector<T> const& other);
			
			Vector<T> operator&(Vector<T> const& other) const;
			
			T dot(Vector<T> const& other) const;
			double angle2(Vector<T> const& other) const;
			
			double lengthSqr() const;
			double length() const;

      void normalize();
      void normalizeLength();
      
      double gaussianKernel(Vector<T> const& other, double bandwidth) const;
	};
	
	////////////////////////////////////////
	// Private member functions:
  template<typename T>
  inline void Vector<T>::copy(Vector<T> const& other)
  {
    d_data = new T[other.d_size];
    d_size = other.d_size;
    memcpy(reinterpret_cast<char*>(d_data), reinterpret_cast<char*>(other.d_data), d_size * sizeof(T));
  }

  template<typename T>
  inline void Vector<T>::destroy()
  {
    if (d_data)
      delete[] d_data;
    
    d_data = 0;
    d_size = 0;
  }
  
	////////////////////////////////////////
	// Public member functions:
  template<typename T>
	inline Vector<T>::Vector()
	: d_data(0), d_size(0)
	{}
	
  template<typename T>
	inline Vector<T>::Vector(unsigned size)
	: d_data(0), d_size(size)
	{
		d_data = new T[size];
		memset(reinterpret_cast<char*>(d_data), 0, d_size * sizeof(T));
	}
	
  template<typename T>
	inline Vector<T>::Vector(Vector<T> const& other)
	{
	  copy(other);
	}
	
  template<typename T>
	inline Vector<T>::~Vector()
	{
	  destroy();
	}
	
  template<typename T>
	inline Vector<T>& Vector<T>::operator=(Vector<T> const& other)
	{
	  if (&other != this)
	  {
	    destroy();
	    copy(other);
	  }
	  return *this;
	}
	
  template<typename T>
	inline bool Vector<T>::operator==(Vector<T> const& other) const
	{
	  assert(d_size == other.d_size);
	  bool eq = true;
	  for (unsigned i = 0; i < d_size; ++i)
	    eq = eq && (d_data[i] == other.d_data[i]);
	  return eq;
	}
	
  template<typename T>
	inline bool Vector<T>::operator!=(Vector<T> const& other) const
  {
    return !(*this == other);
  }
  
  template<typename T>
	inline T Vector<T>::operator[](unsigned idx) const
	{
	  assert(idx < d_size);
	  return d_data[idx];
	}
	
  template<typename T>
	inline T& Vector<T>::operator[](unsigned idx)
	{
	  assert(idx < d_size);
	  return d_data[idx];
	}
	
  template<typename T>
	inline unsigned Vector<T>::size() const
	{
	  return d_size;
	}
	

  template<typename T>
	inline bool Vector<T>::operator<(Vector<T> const& other) const
	{
  	return lengthSqr() < other.lengthSqr();
	}
	
  template<typename T>
	inline Vector<T> Vector<T>::operator+(T val) const
	{
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] + val;
	  return res;
	}
	
  template<typename T>
  inline Vector<T> Vector<T>::operator-(T val) const
	{
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] - val;
	  return res;
	}

  template<typename T>
	inline Vector<T> Vector<T>::operator*(T val) const
	{
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] * val;
	  return res;
	}

  template<typename T>
	inline Vector<T> Vector<T>::operator/(T val) const
	{
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] / val;
	  return res;
	}



  template<typename T>
	inline Vector<T>& Vector<T>::operator+=(T val)
	{
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] += val;
	  return *this;
	}

  template<typename T>
	inline Vector<T>& Vector<T>::operator-=(T val)
	{
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] -= val;
	  return *this;
	}

  template<typename T>
	inline Vector<T>& Vector<T>::operator*=(T val)
	{
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] *= val;
	  return *this;
	}

  template<typename T>
	inline Vector<T>& Vector<T>::operator/=(T val)
	{
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] /= val;
	  return *this;
	}



  template<typename T>
	inline Vector<T> Vector<T>::operator+(Vector<T> const& other) const
	{
	  assert(d_size == other.d_size);
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] + other.d_data[i];
	  return res;
	}

  template<typename T>
	inline Vector<T> Vector<T>::operator-(Vector<T> const& other) const
	{
	  assert(d_size == other.d_size);
	  Vector<T> res(d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    res[i] = d_data[i] - other.d_data[i];
	  return res;
	}
	


  template<typename T>
	inline T Vector<T>::operator*(Vector<T> const& other) const
	{
	  assert(d_size == other.d_size);
	  T res = 0;
	  for (unsigned i = 0; i < d_size; ++i)
	    res += d_data[i] * other.d_data[i];
	  return res;
	}
	


  template<typename T>
	inline Vector<T>& Vector<T>::operator+=(Vector<T> const& other)
	{
	  assert(d_size == other.d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] += other.d_data[i];
	  return *this;
	}

  template<typename T>
	inline Vector<T>& Vector<T>::operator-=(Vector<T> const& other)
	{
	  assert(d_size == other.d_size);
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] -= other.d_data[i];
	  return *this;
	}

  template<typename T>
  inline Vector<T> Vector<T>::operator&(Vector<T> const& other) const
  {
    Vector<T> res(d_size + other.size());
    memcpy(res.d_data, d_data, d_size * sizeof(T));
    memcpy(res.d_data + d_size, other.d_data, other.size() * sizeof(T));
    return res;
  }

  template<typename T>
	inline T Vector<T>::dot(Vector<T> const& other) const
	{
	  assert(d_size == other.d_size);
	  T d = 0;
	  for (unsigned i = 0; i < d_size; ++i)
	    d += d_data[i] * other.d_data[i];
	  return d;
	}
	
  template<typename T>
	inline double Vector<T>::angle2(Vector<T> const& other) const
	{
		assert(d_size == 2 && other.d_size == 2);
	 	return acos(dot(other));
	}
	
  template<typename T>
	inline double Vector<T>::lengthSqr() const
	{
	  return dot(*this);
	}
	
  template<typename T>
	inline double Vector<T>::length() const
	{
	  return sqrt(lengthSqr());
	}
	
	template<typename T>
	inline void Vector<T>::normalize()
	{
	  double s = 0;
	  for (unsigned i = 0; i < d_size; ++i)
	    s += d_data[i];
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] = s > 0 ? d_data[i] / s : 0;
	}
	
	template<typename T>
	inline void Vector<T>::normalizeLength()
	{
	  double l = length();
	  for (unsigned i = 0; i < d_size; ++i)
	    d_data[i] /= l;
	}
	
  template<typename T>
  inline double Vector<T>::gaussianKernel(Vector<T> const& other, double bandwidth) const
  {
    return exp(-(*this - other).lengthSqr()/(bandwidth * bandwidth));
  }
  
	////////////////////////////////////////
	// Global functions:
	
  template<typename T>
  std::ostream& operator<<(std::ostream& out, Vector<T> const& vec)
  {
    int p = out.precision();
    for (unsigned i = 0; i < vec.size(); ++i)
      out << std::setprecision(3) << std::setw(5) << vec[i] << " ";
    out.precision(p);
    return out;
  }
};

#endif

