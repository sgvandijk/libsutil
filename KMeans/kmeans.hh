#ifndef _SUTIL_KMEANS_HH_
#define _SUTIL_KMEANS_HH_

#include <vector>
#include <utility>
#include "../Vector/vector.hh"

namespace sutil
{
  template<typename T>
  class KMeans
  {
      unsigned d_k;
      unsigned d_vectorSize;
      Vector<T> d_min, d_max;
      unsigned d_maxData;
      
      std::vector<Vector<T> > d_means;
      std::vector<T> d_meanLabels;
      
      std::vector<Vector<T> > d_data;
      std::vector<T> d_dataLabels;
      
      std::vector<unsigned> d_classification;
      
    public:
      KMeans(unsigned k, unsigned vectorSize, unsigned maxData);
      
      void setMinMax(Vector<T> const& min, Vector<T> const& max, T label = 0);
      
      std::vector<std::pair<Vector<T>, T> > getMeans();
      void addData(Vector<T> const& data, T label);
      
      void cluster();
      
  };
};

#endif

