#include "kmeans.ih"

template<typename T>
void KMeans<T>::cluster()
{
  

  bool classificationStable = false;
  while (!classificationStable)
  {
    classificationStable = true;
    vector<Vector<T> > newMeans;
    T newLabels[d_k];
    unsigned count[d_k];
    
    for (unsigned i = 0; i < d_k; ++i)
    {
      newMeans.push_back(Vector<T>(d_vectorSize));
      newLabels[i] = 0;
      count[i] = 0;
    }

    // Assign classification to data
    for (unsigned i = 0; i < d_data.size(); ++i)
    {
      double nearestDist = 1e20;
      unsigned cl = 0;
      for (unsigned j = 0; j < d_k; ++j)
      {
        double dist = (d_data[i] - d_means[j]).length();
        if (dist < nearestDist)
        {
          nearestDist = dist;
          cl = j;
        }
      }
      
      if (cl != d_classification[i])
      {
        d_classification[i] = cl;
        classificationStable = false;
      }
      
      newMeans[cl] += d_data[i];
      newLabels[cl] += d_dataLabels[i];
      
      ++count[cl];
    }
          
    
    // Compute new cluster centers and labels
    for (unsigned i = 0; i < d_k; ++i)
      if (count[i] != 0)
      {
        d_means[i] = newMeans[i] / count[i];
        d_meanLabels[i] = newLabels[i] / count[i];
      }
      
    // Repeat
  }
}

