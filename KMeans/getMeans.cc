#include "kmeans.ih"

template<typename T>
vector<pair<Vector<T>, T> > KMeans<T>::getMeans()
{
  vector<pair<Vector<T>, T> > res;
  for (unsigned i = 0; i < d_k; ++i)
  {
    res.push_back(pair<Vector<T>, T>(d_means[i], d_meanLabels[i]));
  }
  
  return res;
}

