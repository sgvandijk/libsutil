#include "kmeans.ih"

template<typename T>
KMeans<T>::KMeans(unsigned k, unsigned vectorSize, unsigned maxData)
  : d_k(k),
    d_vectorSize(vectorSize),
    d_maxData(maxData)
{
}

