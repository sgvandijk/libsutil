#include "kmeans.ih"

template<typename T>
void KMeans<T>::addData(Vector<T> const& data, T label)
{
  d_data.push_back(data);
  d_dataLabels.push_back(label);
  d_classification.push_back((unsigned)(d_k * (double)(rand() + 1) / (double)RAND_MAX));
  if (d_data.size() > d_maxData)
  {
    d_data.erase(d_data.begin());
    d_dataLabels.erase(d_dataLabels.begin());
    d_classification.erase(d_classification.begin());
  }
}

