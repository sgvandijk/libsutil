#include "kmeans.ih"

template<typename T>
void KMeans<T>::setMinMax(Vector<T> const& min, Vector<T> const& max, T label)
{
  d_min = min;
  d_max = max;
  
  // Assign random means
  d_means.clear();
  for (unsigned i = 0; i < d_k; ++i)
  {
    Vector<T> mean(d_vectorSize);
    for (unsigned j = 0; j < d_vectorSize; ++j)
      mean[j] = (T)((d_max[j] - d_min[j]) * (T)(rand() + 1) / (double)RAND_MAX + d_min[j]);
    d_means.push_back(mean);
    d_meanLabels.push_back(label);
  }
}

