#include "svgimage.hh"
#include <sstream>

using namespace sutil;
using namespace std;

string SvgImage::RGB(double r, double g, double b)
{
  ostringstream out;
  out << "rgb(" << (unsigned)(255 * r) << "," << (unsigned)(255 * g) << "," << (unsigned)(255 * b) << ")";
  return out.str();
}

