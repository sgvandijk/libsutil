#include "svgimage.hh"

using namespace sutil;

void SvgImage::copy(SvgImage const& other)
{
  d_width = other.d_width;
  d_height = other.d_height;
  for (std::vector<Definition*>::const_iterator iter = other.d_defs.begin(); iter != other.d_defs.end(); ++iter)
    d_defs.push_back((*iter)->clone());

  for (std::vector<Shape*>::const_iterator iter = other.d_shapes.begin(); iter != other.d_shapes.end(); ++iter)
    d_shapes.push_back((*iter)->clone());
}

