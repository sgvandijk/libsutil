#ifndef _SUTIL_SVGIMAGE_HH_
#define _SUTIL_SVGIMAGE_HH_

#include "../Shape/shape.hh"
#include "../Definition/definition.hh"

#include <vector>
#include <ostream>

namespace sutil
{
  class SvgImage
  {
  private:
    Size d_width, d_height;
    std::vector<Definition*> d_defs;
    std::vector<Shape*> d_shapes;
    
    void copy(SvgImage const& other);
      
    void destroy()
    {
      for (unsigned i = 0; i < d_shapes.size(); ++i)
        delete d_shapes[i];
      for (unsigned i = 0; i < d_defs.size(); ++i)
        delete d_defs[i];
    }
      
  public:
    SvgImage()
    {
    }
      
    SvgImage(Size width, Size height)
      : d_width(width),
        d_height(height)
    {
    }
      
    SvgImage(SvgImage const& other)
    {
      copy(other);
    }
      
    ~SvgImage()
    {
      destroy();
    }
    
    SvgImage& operator=(SvgImage const& other)
    {
      if (&other != this)
      {
        destroy();
        copy(other);
      }
      return *this;
    }
      
    Size getWidth() const { return d_width; }
    Size getHeight() const { return d_height; }
      
    std::vector<Shape*> getShapes() const { return d_shapes; }
    std::vector<Shape*>& getShapes() { return d_shapes; }
    void addShape(Shape* shape) { d_shapes.push_back(shape); }
      
    std::vector<Definition*> getDefinitions() const { return d_defs; }
    std::vector<Definition*>& getDefinitions() { return d_defs; }
    void addDefinition(Definition* definition) { d_defs.push_back(definition); }
      
    void clear()
    {
      for (unsigned i = 0; i < d_shapes.size(); ++i)
        delete d_shapes[i];
      d_shapes.clear();
      for (unsigned i = 0; i < d_defs.size(); ++i)
        delete d_defs[i];
      d_defs.clear();
    }
      
    static std::string RGB(double r, double g, double b);
    static std::string HSV(double h, double s, double v);
      
    static void outputTikz(std::ostream& out, SvgImage const& img);
    static void outputTikz(std::ostream& out, Size const& size);
    static void outputTikz(std::ostream& out, std::shared_ptr<Color> col);
      
    static void outputTikz(std::ostream& out, Shape const& shape);
    static void outputTikz(std::ostream& out, Line const& line);
    static void outputTikz(std::ostream& out, Arrow const& line);
    static void outputTikz(std::ostream& out, Rect const& rect);
    static void outputTikz(std::ostream& out, Circle const& circle);
    static void outputTikz(std::ostream& out, Polygon const& polygon);
    static void outputTikz(std::ostream& out, Polyline const& polyline);
    static void outputTikz(std::ostream& out, Text const& text);
      
    static void outputTikz(std::ostream& out, Definition const& definition);
    static void outputTikz(std::ostream& out, GradientStop const& stop);
    static void outputTikz(std::ostream& out, LinearGradient const& gradient);
    static void outputTikz(std::ostream& out, RadialGradient const& gradient);
    static void outputTikz(std::ostream& out, Pattern const& pattern);
  };
  
  std::ostream& operator<<(std::ostream& out, SvgImage const& img);
  
  std::ostream& operator<<(std::ostream& out, Size const& size);
  std::ostream& operator<<(std::ostream& out, std::shared_ptr<Color> col);
  std::ostream& operator<<(std::ostream& out, Shape const& shape);
  std::ostream& operator<<(std::ostream& out, Line const& line);
  std::ostream& operator<<(std::ostream& out, Arrow const& line);
  std::ostream& operator<<(std::ostream& out, Rect const& rect);
  std::ostream& operator<<(std::ostream& out, Circle const& circle);
  std::ostream& operator<<(std::ostream& out, Polygon const& polygon);
  std::ostream& operator<<(std::ostream& out, Polyline const& polyline);
  std::ostream& operator<<(std::ostream& out, Text const& text);
  
  std::ostream& operator<<(std::ostream& out, Definition const& definition);
  std::ostream& operator<<(std::ostream& out, GradientStop const& stop);
  std::ostream& operator<<(std::ostream& out, LinearGradient const& gradient);
  std::ostream& operator<<(std::ostream& out, RadialGradient const& gradient);
  std::ostream& operator<<(std::ostream& out, Pattern const& pattern);
};

#endif

