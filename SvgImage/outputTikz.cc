#include "svgimage.hh"
#include <iostream>
#include <cmath>

using namespace sutil;
using namespace std;
//using namespace boost;

void SvgImage::outputTikz(ostream& out, SvgImage const& img)
{
  out << "\\begin{tikzpicture}" << endl;
  
  /*
  vector<Definition*> defs = img.getDefinitions();
  if (defs.size() > 0)
  {
    out << "<defs>" << endl;
    for (vector<Definition*>::iterator iter = defs.begin(); iter != defs.end(); ++iter)
      out << *(*iter);
    out << "</defs>" << endl;
  }
  */
  
  vector<Shape*> shapes = img.getShapes();
  for (vector<Shape*>::iterator iter = shapes.begin(); iter != shapes.end(); ++iter)
    outputTikz(out, *(*iter));

  out << "\\end{tikzpicture}" << endl;
  
}

void SvgImage::outputTikz(ostream& out, Shape const& shape)
{
  /*
  Line const* line = dynamic_cast<Line const*>(&shape);
  if (line)
    outputTikz(out, *line);
  */
  
  Rect const* rect = dynamic_cast<Rect const*>(&shape);
  if (rect)
    outputTikz(out, *rect);

  Arrow const* arrow = dynamic_cast<Arrow const*>(&shape);
  if (arrow)
    outputTikz(out, *arrow);
  
  Polygon const* polygon = dynamic_cast<Polygon const*>(&shape);
  if (polygon)
    outputTikz(out, *polygon);

  Circle const* circle = dynamic_cast<Circle const*>(&shape);
  if (circle)
    outputTikz(out, *circle);

/*
  Polyline const* polyline = dynamic_cast<Polyline const*>(&shape);
  if (polyline)
    outputTikz(out, *polyline);
  
  Text const* text = dynamic_cast<Text const*>(&shape);
  if (text)
    outputTikz(out, *text);
*/
}

void SvgImage::outputTikz(ostream& out, Size const& size)
{
  out << (size.val / 100);
}

void SvgImage::outputTikz(ostream& out, shared_ptr<Color> col)
{
  shared_ptr<StrColor> strc = dynamic_pointer_cast<StrColor>(col);
  if (strc)
  {
    out << strc->str;
    return;
  }

  shared_ptr<RGBColor> rgbc = dynamic_pointer_cast<RGBColor>(col);
  if (rgbc)
  {
    
    out << "{rgb,1:red," << (rgbc->r > 1e-3 ? rgbc->r : 0) << ";green," << (rgbc->g > 1e-3 ? rgbc->g : 0) << ";blue," << (rgbc->b > 1e-3 ? rgbc->b : 0) << "}";
    return;
  }

  shared_ptr<HSVColor> hsvc = dynamic_pointer_cast<HSVColor>(col);
  if (hsvc)
  {
    double vh = hsvc->h * 6;
    double vi = (int)(vh);
    double v1 = hsvc->v * (1.0 - hsvc->s);
    double v2 = hsvc->v * (1.0 - hsvc->s * (vh - vi));
    double v3 = hsvc->v * (1.0 - hsvc->s * (1.0 - (vh -vi)));

    double r, g, b;
    
    if      ( vi == 0 ) { r = hsvc->v  ; g = v3 ; b = v1 ; }
    else if ( vi == 1 ) { r = v2 ; g = hsvc->v  ; b = v1 ; }
    else if ( vi == 2 ) { r = v1 ; g = hsvc->v  ; b = v3 ; }
    else if ( vi == 3 ) { r = v1 ; g = v2 ; b = hsvc->v  ; }
    else if ( vi == 4 ) { r = v3 ; g = v1 ; b = hsvc->v  ; }
    else                { r = hsvc->v  ; g = v1 ; b = v2 ; }

    out << "{rgb,1:red," << (r > 1e-3 ? r : 0) << ";green," << (g > 1e-3 ? g : 0) << ";blue," << (b > 1e-3 ? b : 0) << "}";
    return;
  }
}


void SvgImage::outputTikz(ostream& out, Line const& line)
{
  out << "\\draw[" << line.stroke << "] (";
  outputTikz(out, line.x1);
  out << ",";
  outputTikz(out, -line.y1);
  out << ") -- (";
  outputTikz(out, line.x2);
  out << ",";
  outputTikz(out, -line.y2);
  out << ");" << endl;

}

void SvgImage::outputTikz(ostream& out, Rect const& rect)
{
  if (rect.fill && rect.stroke != "")
  {
    out << "\\filldraw[draw=" << rect.stroke << ",fill=";
    outputTikz(out, rect.fill);
    if (rect.opacity < 1)
      out << ",fill opacity=" << rect.opacity;
    out <<"]";
  }
  else if (!rect.fill)
  {
    out << "\\draw[" << rect.stroke;
    if (rect.pattern != "")
      out << ",pattern=" << rect.pattern;
    out << "]";
  }
  else
  {
    out << "\\fill[";
    outputTikz(out, rect.fill);
    if (rect.opacity < 1)
      out << ",fill opacity=" << rect.opacity;
    out << "]";
  }
  
  out << " (";
  outputTikz(out, rect.x);
  out << ",";
  outputTikz(out, -rect.y);
  out << ") rectangle (";
  outputTikz(out, rect.x + rect.width);
  out << ",";
  outputTikz(out, -rect.y - rect.height);
  out << ");" << endl;
}

void SvgImage::outputTikz(ostream& out, Polygon const& polygon)
{
  if (polygon.fill && polygon.stroke != "")
  {
    out << "\\filldraw[draw=" << polygon.stroke << ",fill=";
    outputTikz(out, polygon.fill);
    if (polygon.opacity < 1)
      out << ",fill opacity=" << polygon.opacity;
    out <<"]";
  }
  else if (!polygon.fill)
    out << "\\draw[" << polygon.stroke << "]";
  else
  {
    out << "\\fill[";
    outputTikz(out, polygon.fill);
    if (polygon.opacity < 1)
      out << ",fill opacity=" << polygon.opacity;
    out << "]";
  }

  for (vector<pair<Size, Size> >::const_iterator iter = polygon.points.begin(); iter != polygon.points.end(); ++iter)
  {
    out << " (";
    outputTikz(out, iter->first);
    out << ",";
    outputTikz(out, -iter->second);
    out << ") --";
  }
  out << " cycle;" << endl;
}

void SvgImage::outputTikz(ostream& out, Arrow const& arrow)
{
  double dirx = arrow.x2.val - arrow.x1.val;
  double diry = arrow.y2.val - arrow.y1.val;
  double length = sqrt(dirx * dirx + diry * diry);
  if (length > 0)
  {
    dirx /= length;
    diry /= length;
  }
  double x = arrow.x.val;
  double y = arrow.y.val;
  
  double perpx = diry;
  double perpy = -dirx;

  //cerr << "dir: " << dirx << " " << diry << ", perp: " << perpx << " " << perpy << endl;
  
  double h1x = arrow.x2.val - dirx * arrow.headSize.val + 0.5 * perpx * arrow.headSize.val;
  double h1y = arrow.y2.val - diry * arrow.headSize.val + 0.5 * perpy * arrow.headSize.val;

  double h2x = arrow.x2.val - dirx * arrow.headSize.val - 0.5 * perpx * arrow.headSize.val;
  double h2y = arrow.y2.val - diry * arrow.headSize.val - 0.5 * perpy * arrow.headSize.val;

  double h3x = arrow.x2.val - dirx * arrow.headSize.val;
  double h3y = arrow.y2.val - diry * arrow.headSize.val;

  Polygon a;
  a.points.push_back(pair<Size, Size>(arrow.x1 * arrow.scale + x, arrow.y1 * arrow.scale + y));
  a.points.push_back(pair<Size, Size>(h3x * arrow.scale + x, h3y * arrow.scale + y));
  a.points.push_back(pair<Size, Size>(h1x * arrow.scale + x, h1y * arrow.scale + y));
  a.points.push_back(pair<Size, Size>(arrow.x2 * arrow.scale + x, arrow.y2 * arrow.scale + y));
  a.points.push_back(pair<Size, Size>(h2x * arrow.scale + x, h2y * arrow.scale + y));
  a.points.push_back(pair<Size, Size>(h3x * arrow.scale + x, h3y * arrow.scale + y));
  a.stroke = arrow.stroke;
  a.strokeWidth = arrow.strokeWidth;
  a.fill = arrow.fill;
  a.style = arrow.style;
  outputTikz(out, a);
  
}

void SvgImage::outputTikz(ostream& out, Circle const& circle)
{
  if (circle.fill && circle.stroke != "")
  {
    out << "\\filldraw[draw=" << circle.stroke << ",fill=";
    outputTikz(out, circle.fill);
    out <<"]";
  }
  else if (!circle.fill)
  {
    out << "\\draw[" << circle.stroke;
    if (circle.pattern != "")
      out << ",pattern=" << circle.pattern;
    out << "]";
  }
  else
  {
    out << "\\fill[";
    outputTikz(out, circle.fill);
    out << "]";
  }
  
  out << " (";
  outputTikz(out, circle.centrX);
  out << ",";
  outputTikz(out, -circle.centrY);
  out << ") circle (";
  outputTikz(out, circle.radius);
  out << ");" << endl;
}

/*

ostream& sutil::operator<<(ostream& out, Arrow const& arrow)
{
  double dirx = arrow.x2.val - arrow.x1.val;
  double diry = arrow.y2.val - arrow.y1.val;
  double length = sqrt(dirx * dirx + diry * diry);
  if (length > 0)
  {
    dirx /= length;
    diry /= length;
  }
  
  double perpx = diry;
  double perpy = -dirx;

  //cerr << "dir: " << dirx << " " << diry << ", perp: " << perpx << " " << perpy << endl;
  
  double h1x = arrow.x2.val - dirx * arrow.headSize.val + 0.5 * perpx * arrow.headSize.val;
  double h1y = arrow.y2.val - diry * arrow.headSize.val + 0.5 * perpy * arrow.headSize.val;

  double h2x = arrow.x2.val - dirx * arrow.headSize.val - 0.5 * perpx * arrow.headSize.val;
  double h2y = arrow.y2.val - diry * arrow.headSize.val - 0.5 * perpy * arrow.headSize.val;

  double h3x = arrow.x2.val - dirx * arrow.headSize.val;
  double h3y = arrow.y2.val - diry * arrow.headSize.val;

  Polygon a;
  a.points.push_back(pair<Size, Size>(arrow.x1, arrow.y1));
  a.points.push_back(pair<Size, Size>(h3x, h3y));
  a.points.push_back(pair<Size, Size>(h1x, h1y));
  a.points.push_back(pair<Size, Size>(arrow.x2, arrow.y2));
  a.points.push_back(pair<Size, Size>(h2x, h2y));
  a.points.push_back(pair<Size, Size>(h3x, h3y));
  a.stroke = arrow.stroke;
  a.strokeWidth = arrow.strokeWidth;
  a.fill = arrow.fill;
  a.style = arrow.style;
  out << a;
  
  return out;
}

ostream& sutil::operator<<(ostream& out, Polyline const& polyline)
{
  out << "<polyline points=\"";
  for (vector<pair<Size, Size> >::const_iterator iter = polyline.points.begin(); iter != polyline.points.end(); ++iter)
  {
    out << iter->first << "," << iter->second << " ";
  }
  out << "\"";
  if (polyline.fill != "")
    out << " fill=\"" << polyline.fill << "\"";
  if (polyline.stroke != "")
    out << " stroke=\"" << polyline.stroke << "\"";
  if (polyline.strokeWidth != 0)
    out << " stroke-width=\"" << polyline.strokeWidth << "\"";
  if (polyline.style != "")
    out << " style=\"" << polyline.style << "\"";
  
  out << "/>" << endl;
  return out;
}

ostream& sutil::operator<<(ostream& out, Text const& text)
{
  out << "<text x=\"" << text.x << "\" y=\"" << text.y << "\" font-family=\"" << text.fontFamily << "\" font-size=\"" << text.fontSize << "\"";

  if (text.fill != "")
    out << " fill=\"" << text.fill << "\"";
  if (text.stroke != "")
    out << " stroke=\"" << text.stroke << "\"";
  if (text.strokeWidth != 0)
    out << " stroke-width=\"" << text.strokeWidth << "\"";
  if (text.style != "")
    out << " style=\"" << text.style << "\"";
  if (text.textAnchor != "")
    out << " text-anchor=\"" << text.textAnchor << "\"";
  if (text.alignmentBaseline != "")
    out << " alignment-baseline=\"" << text.alignmentBaseline << "\"";
  if (text.baselineShift != "")
    out << " baseline-shift=\"" << text.baselineShift << "\"";
    
  out << ">" << text.content << "</text>" << endl;
  return out;
}

ostream& sutil::operator<<(ostream& out, Definition const& definition)
{
  LinearGradient const* linGradient = dynamic_cast<LinearGradient const*>(&definition);
  if (linGradient)
  {
    out << *linGradient;
    return out;
  }

  RadialGradient const* radGradient = dynamic_cast<RadialGradient const*>(&definition);
  if (radGradient)
  {
    out << *radGradient;
    return out;
  }
  
  Pattern const* pattern = dynamic_cast<Pattern const*>(&definition);
  if (pattern)
  {
    out << *pattern;
    return out;
  }
  
  return out;
}

ostream& sutil::operator<<(ostream& out, GradientStop const& stop)
{
  out << "<stop offset=\"" << stop.offset << "\" stop-color=\"" << stop.color << "\" stop-opacity=\"" << stop.opacity << "\"/>" << endl;
  return out;
}

ostream& sutil::operator<<(ostream& out, LinearGradient const& gradient)
{
  out << "<linearGradient id=\"" << gradient.id << "\" x1=\"" << gradient.x1 << "\" y1=\"" << gradient.y1 << "\" x2=\"" << gradient.x2 << "\" y2=\"" << gradient.y2 << "\">" << endl;
  for (unsigned i = 0; i < gradient.stops.size(); ++i)
    out << gradient.stops[i];
  out << "</linearGradient>" << endl;
  return out;
}

ostream& sutil::operator<<(ostream& out, RadialGradient const& gradient)
{
  out << "<radialGradient id=\"" << gradient.id << "\" cx=\"" << gradient.centrX << "\" cy=\"" << gradient.centrY << "\" r=\"" << gradient.radius << "\">" << endl;
  for (unsigned i = 0; i < gradient.stops.size(); ++i)
    out << gradient.stops[i];
  out << "</radialGradient>" << endl;
  return out;
}

ostream& sutil::operator<<(ostream& out, Pattern const& pattern)
{
  out << "<pattern id=\"" << pattern.id << "\" patternUnits=\"" << pattern.patternUnits << "\" x=\"" << pattern.x << "\" y=\"" << pattern.y << "\" width=\"" << pattern.width << "\" height=\"" << pattern.height << "\">" << endl;
  for (unsigned i = 0; i < pattern.shapes.size(); ++i)
    out << *pattern.shapes[i];
  out << "</pattern>" << endl;
  return out;
}
*/

