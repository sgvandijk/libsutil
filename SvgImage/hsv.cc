#include "svgimage.hh"

using namespace sutil;
using namespace std;

string SvgImage::HSV(double h, double s, double v)
{
  double vh = h * 6;
  double vi = (int)(vh);
  double v1 = v * (1.0 - s);
  double v2 = v * (1.0 - s * (vh - vi));
  double v3 = v * (1.0 - s * (1.0 - (vh -vi)));

  double r, g, b;
  
  if      ( vi == 0 ) { r = v  ; g = v3 ; b = v1 ; }
  else if ( vi == 1 ) { r = v2 ; g = v  ; b = v1 ; }
  else if ( vi == 2 ) { r = v1 ; g = v  ; b = v3 ; }
  else if ( vi == 3 ) { r = v1 ; g = v2 ; b = v  ; }
  else if ( vi == 4 ) { r = v3 ; g = v1 ; b = v  ; }
  else                { r = v  ; g = v1 ; b = v2 ; }

  return RGB(r, g, b);
}

