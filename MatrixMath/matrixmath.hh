#ifndef _SUTIL_MATRIXMATH_
#define _SUTIL_MATRIXMATH_

#include "../Matrix/matrix.hh"
#include "../Vector/vector.hh"

namespace sutil
{
  template<typename T>
  class MatrixMath
  {
    private:
      MatrixMath(); //NI
      MatrixMath(MatrixMath<T> const& other); //NI
      MatrixMath& operator=(MatrixMath<T> const& other); //NI
      
    public:
      static Vector<T> linearRegression(Matrix<T> const& X, Vector<T> const& y);
      static Vector<T> locallyWeightedRegression(Matrix<T> const& X, Vector<T> const& y, Vector<T> const& query, double bandWidthSqr);

      static double kernel(double distSqr, double bandWidthSqr)
      {
        return exp(-distSqr/bandWidthSqr);
      }
  };

  template<typename T>
  Vector<T> MatrixMath<T>::linearRegression(Matrix<T> const& X, Vector<T> const& y)
  {
    assert(X.rows() == y.size());
    
    Matrix<T> Xt = X.transpose();
    return (Xt * X).inverse() * Xt * y;
  }

  template<typename T>
  Vector<T> MatrixMath<T>::locallyWeightedRegression(Matrix<T> const& X, Vector<T> const& y, Vector<T> const& query, double bandWidthSqr)
  {
    Vector<T> queryExt(query.size() + 1);
    queryExt[0] = 1;
    for (unsigned i = 0; i < query.size(); ++i)
      queryExt[i + 1] = query[i];
    
    Matrix<T> Xw = X;
    Vector<T> yw = y;
    
    for (unsigned i = 0; i < X.rows(); ++i)
    {
      Vector<T> diff = queryExt - X[i];
      double d = diff.dot(diff);
      double k = kernel(d, bandWidthSqr);
      Xw[i] = Xw[i] * k;
      yw[i] = yw[i] * k;
    }
    
    return linearRegression(Xw, yw);
  }
};

#endif

