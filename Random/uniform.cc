#include "random.ih"

double Random::uniformDbl(double begin, double end)
{
  return 1.0 * rand() / (RAND_MAX + 1.0) * (end - begin) + begin;
}

int Random::uniformInt(int b, int e)
{
  long int begin = b;
  long int end = e;
  return (rand() % (end - begin)) + begin;
}
