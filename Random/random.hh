#ifndef __SUTIL_RANDOM_HH__
#define __SUTIL_RANDOM_HH__

#include <Eigen/Core>

namespace sutil
{
  class Random
  {
    public:
      static void seed(unsigned s);

      static double uniform(double begin = 0, double end = 1) { return uniformDbl(begin, end); }

      static double uniformDbl(double begin = 0, double end = 1);

      static int uniformInt(int begin, int end);

      static Eigen::VectorXi uniformSeries(int n, int begin, int end, bool replacement = true);

      static double stdNorm();

    private:
      Random (); // NI

  };
};


#endif /* __SUTIL_RANDOM_HH__ */

