#include "random.ih"

VectorXi Random::uniformSeries(int n, int begin, int end, bool replacement)
{
  VectorXi res(n);
  res.fill(begin - 1);
  for (int i = 0; i < n; ++i)
  {
    int draw;
    do
      draw = uniformInt(begin, end);
    while (!replacement && (res.array() == draw).sum() != 0);
    res(i) = draw;
  }
  return res;
}
